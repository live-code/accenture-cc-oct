import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NavbarComponent } from './core/components/navbar.component';
import { SettingsModule } from './features/settings/settings.module';
import { ContactsModule } from './features/contacts/contacts.module';
import { UsersDetailsModule } from './features/users-details/users-details.module';
import { AppRoutingModule } from './app-routing.module';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent, NavbarComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SettingsModule,
    ContactsModule,
    UsersDetailsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [

  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
