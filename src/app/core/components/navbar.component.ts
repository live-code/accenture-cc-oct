import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div 
      style="padding: 20px"
      [style.backgroundColor]="theme === 'dark' ? '#222' : '#ccc'"
    >
      <button routerLink="login" routerLinkActive="bg-dark text-white">login</button> |
      <button routerLink="home" routerLinkActive="bg-dark text-white">home</button> |
      <button routerLink="contacts" routerLinkActive="bg-dark text-white">contacts</button> |
      <button routerLink="settings" routerLinkActive="bg-dark text-white">settings</button> |
      <button routerLink="users" routerLinkActive="bg-dark text-white">users</button> |
      
      <button (click)="translate.use('it')">IT</button>
      <button (click)="translate.use('en')">EN</button>
    </div>
  `,
})
export class NavbarComponent {
  @Input() theme: string;

  constructor(public translate: TranslateService)  {
    translate.setDefaultLang('en');
  }
}
