import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})
export class ThemeService {
  theme: 'dark' | 'light' = 'dark';

  setTheme(value: 'dark' | 'light'): void {
    // ...
    this.theme = value;
  }

  getTheme(): string {
    // ...
    return this.theme;
  }
}
