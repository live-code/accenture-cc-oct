import { Component } from '@angular/core';
import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar [theme]="themeService.theme"></app-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  constructor(public themeService: ThemeService) {
  }
}
