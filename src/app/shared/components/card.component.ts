
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div class="card-header" (click)="isOpen = !isOpen">
        {{title}}
        <i *ngIf="icon" 
           class="pull-right" 
           [ngClass]="icon" 
           (click)="iconHandler($event)"></i>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() url: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>();
  isOpen = true;

  iconHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit();
  }
}
