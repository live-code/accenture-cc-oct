import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item" *ngFor="let item of items">
        <a class="nav-link"
           [ngClass]="{'active': item.id === active?.id}"
           (click)="tabClick.emit(item)">
          {{item.label}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[];
  @Input() active: any;
  @Output() tabClick: EventEmitter<any> = new EventEmitter<any>();
}


// toggable card - list item
 // list item
