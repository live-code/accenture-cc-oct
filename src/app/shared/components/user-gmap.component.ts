import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-users-gmap',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div *ngIf="city">
      <label *ngIf="displayName">{{city}}</label>
      <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=100x50&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </div>
  `,
})
export class UserGmapComponent {
  @Input() city: string;
  @Input() displayName = false;
}
