import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { UserGmapComponent } from './components/user-gmap.component';



@NgModule({
  declarations: [
    CardComponent,
    TabbarComponent,
    UserGmapComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    TabbarComponent,
    UserGmapComponent,
  ]
})
export class SharedModule { }
