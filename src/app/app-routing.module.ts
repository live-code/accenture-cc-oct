import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContactsComponent } from './features/contacts/contacts.component';
import { SettingsComponent } from './features/settings/settings.component';
import { UsersDetailsComponent } from './features/users-details/users-details.component';
import { HomeComponent } from './features/home/home.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: 'contacts', component: ContactsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)},
      { path: 'users/:id', component: UsersDetailsComponent },
      { path: '',  loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: '**', redirectTo: ''}
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
