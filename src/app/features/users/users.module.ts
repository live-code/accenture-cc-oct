import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UserNameComponent } from './components/user-name.component';
import { IconGenderComponent } from './components/icon-gender.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFormComponent } from './components/users-form.component';
import { UsersListItemComponent } from './components/users-list-item.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    UsersComponent,
    UserNameComponent,
    IconGenderComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    TranslateModule,
    RouterModule.forChild([
      { path: '', component: UsersComponent},
    ])
  ]
})
export class UsersModule {

}
