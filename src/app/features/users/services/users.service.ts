import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, EMPTY, Observable, of, pipe, Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { share, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root'})
export class UsersService {
  users: User[];
  active: User | null;

  constructor(private http: HttpClient) {
    this.resetHandler();
  }

  getAllUsers(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe((result) => {
        this.users = result;
      });
  }


  deleteHandler(user: User): void {
    this.http.delete('http://localhost:3000/users/' + user.id)
      .subscribe(() => {
        this.users = this.users.filter(u => u.id !== user.id)

        if (this.active?.id === user.id) {
          this.resetHandler();
        }
      });
  }

  saveHandler(user: User): void {
      if (this.active?.id) {
        this.editHandler(user);
      } else {
        this.addHandler(user);
      }
  }

  addHandler(user: User): void {
    this.http.post<User>('http://localhost:3000/users', user)
     .subscribe(res => {

       this.users = [...this.users, res]
       this.resetHandler();
     });
  }

  editHandler(user: User): void {
    if (this.active) {
      this.http.patch<User>('http://localhost:3000/users/' + this.active.id, user)
        .subscribe(res => {
          this.users = this.users.map(u => {
            return u.id === this.active?.id ? {...u, ...user} : u;
          });
        });
    }

  }

  setActiveHandler(user: User): void {
    this.active = user;
  }

  resetHandler(): void {
    this.active = { gender: null } as User;
    // f.reset();
  }
}
