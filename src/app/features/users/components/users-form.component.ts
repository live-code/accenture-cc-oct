import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form #f="ngForm" (submit)="saveHandler()">
      <div *ngIf="inputName.errors?.required">Obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">
        Missing {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} chars
      </div>

      <input
        class="form-control"
        [ngClass]="{
          'is-invalid': inputName.invalid && f.dirty, 
          'is-valid': inputName.valid
        }"
        type="text"
        [ngModel]="active?.name"
        #inputName="ngModel"
        name="name" placeholder="name"
        required
        minlength="5"
      >

      <input type="text" [ngModel]="active?.age" name="age" placeholder="age" class="form-control">

      <select
        [ngModel]="active?.gender"
        name="gender" required
        class="form-control"
        [ngClass]="{'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid}"
        #inputGender="ngModel"
      >
        <option [ngValue]="null">Select a gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <button type="submit" [disabled]="f.invalid">SAVE</button>
      <button type="button" (click)="resetHandler()" *ngIf="active">RESET</button>
    </form>
    {{update()}}

    {{active | json}}
  `,
})
export class UsersFormComponent implements OnChanges {
  @ViewChild('f') form: NgForm;
  @Input() active: User;
  @Input() color: string;
  @Output() saveUser: EventEmitter<User> = new EventEmitter<User>()
  @Output() reset: EventEmitter<void> = new EventEmitter<void>()

  constructor(private changeDetector: ChangeDetectorRef) {
    setTimeout(() => changeDetector.detectChanges());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.active && !changes.active.currentValue.id) {
      this.form?.reset();
    }
  }

  saveHandler(): void {
    this.saveUser.emit(this.form.value);
  }

  resetHandler(): void {
    this.form.reset();
    this.reset.emit();
  }


  update() {
    console.log('update: form')
  }

  ngOnDestroy() {
    console.log('destroey')
  }


}
