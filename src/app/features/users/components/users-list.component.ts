import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-users-list-item
      *ngFor="let user of users"
      [user]="user"
      [active]="active"
      (deleteUser)="deleteUser.emit($event)"
      (setActive)="setActive.emit($event)"
    ></app-users-list-item>
    
  `,
})
export class UsersListComponent{
  @Input() users: User[];
  @Input() active: User;
  @Output() setActive: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteUser: EventEmitter<User> = new EventEmitter<User>();
}
