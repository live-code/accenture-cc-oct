import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list-item',
  template: `
    <div
      
      class="list-group-item"
      (click)="setActive.emit(user)"
      [ngClass]="{
          'male': user.gender === 'M',
          'female': user.gender === 'F'
        }"
      [style.border]="user.id === active?.id ? '5px solid black' : null"
    >
      <app-icon-gender [value]="user.gender"></app-icon-gender>
      <!--<app-user-name [value]="user.name" [size]="3" priority="success"> </app-user-name>-->
      {{user.name}}
      <div class="pull-right">
        <i class="fa mr-2 fa-arrow-circle-down " (click)="toggleHandler($event)"></i>
        <i class="fa mr-2 fa-link" [routerLink]="'/users/' + user.id"></i>
        <i class="fa mr-2 fa-trash"
           (click)="deleteHandler($event, user)"></i>
      </div>

      <div *ngIf="isOpen">
        <app-users-gmap [city]="user.city" *ngIf="user.city"></app-users-gmap>
      </div>
    </div>
  `,
})
export class UsersListItemComponent {
  isOpen = false;
  @Input() user: User;
  @Input() active: User;
  @Output() setActive: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteUser: EventEmitter<User> = new EventEmitter<User>();

  toggleHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.isOpen = !this.isOpen;
  }

  deleteHandler(event: MouseEvent, user: User): void {
    event.stopPropagation();
    this.deleteUser.emit(user);
  }
}
