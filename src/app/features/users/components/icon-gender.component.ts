import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon-gender',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-mars': value === 'M',
        'fa-venus': value === 'F'
      }"
    ></i>
  `,
  styles: [
  ]
})
export class IconGenderComponent {
  @Input() value: 'M' | 'F' | null = null;
}
