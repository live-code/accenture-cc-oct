import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-name',
  template: `
    <div 
      *ngIf="value"
      class="alert" 
      [ngClass]="'alert-' + priority"
      [style.fontSize.px]="size * 10"
    >
      {{value}}
    </div>
    
    <div *ngIf="!value">Il nome è required</div>
  `,
})
export class UserNameComponent  {
  @Input() value;
  @Input() size: 1 | 2 | 3 = 1;
  @Input() priority: 'alert' | 'info' | 'success';

}
