import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users',
  template: `
    <hr>
   
    
    <app-users-form
      [active]="userService.active"
      color="red"
      (saveUser)="userService.saveHandler($event)"
      (reset)="userService.resetHandler()"
    ></app-users-form>
    <hr>
    <div class="container mt-3">
      <app-users-list
        [users]="userService.users"
        [active]="userService.active"
        (setActive)="userService.setActiveHandler($event)"
        (deleteUser)="userService.deleteHandler($event)"
      ></app-users-list>
    </div>
  `,
})
export class UsersComponent {
  constructor(public userService: UsersService)  {
    this.userService.getAllUsers();
  }
}
