import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <h1>Settings Page</h1>
    <button (click)="themeService.setTheme('light')">light</button>
    <button (click)="themeService.setTheme('dark')">dark</button>
  `,
})
export class SettingsComponent {

  constructor(public themeService: ThemeService) { }

}
