import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'app-users-details',
  template: `
    <h1>{{data?.name}}</h1>
    <h2>{{data?.age}}</h2>
    
    <button routerLink="/users">back to list</button>
  `,
})
export class UsersDetailsComponent implements OnInit {
  data: User;

  constructor(
    private activatedRoute: ActivatedRoute,
    http: HttpClient
  ) {
    const id = activatedRoute.snapshot.params.id;
    http.get<User>('http://localhost:3000/users/' + id)
      .subscribe(res => this.data = res)
  }

  ngOnInit(): void {
  }

}
