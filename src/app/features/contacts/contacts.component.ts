import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  template: `
    <app-card title="Location" icon="fa fa-map" (iconClick)="state = 1" > 
<!--
      <app-users-gmap city="Trieste"></app-users-gmap>
-->
    </app-card>
    
    <app-card title="contacts form!" icon="fa fa-envelope" (iconClick)="doSomething()">
      <input type="text">
      <input type="text">
      <input type="text">
      <input type="text">
    </app-card>
    
    <app-card title="ciao">
      <div class="row">
        <div class="col">
          <app-card title="left"></app-card>
        </div>
        <div class="col">
          <app-card title="right"></app-card>
        </div>
      </div>
    </app-card>
  `,
  styles: [
  ]
})
export class ContactsComponent {
  state = 0;

  openMap(): void {
    window.open('http://www.google.com')
  }

  doSomething(): void {
    alert('ciao')
  }
}
