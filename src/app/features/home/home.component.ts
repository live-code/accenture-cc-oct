import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    {{'WELCOME' | translate}}
    {{'HELLO' | translate: { value: 'pippo'} }}
    
    <app-tabbar 
      [items]="countries" 
      [active]="activeCountry"
      (tabClick)="setActiveCountry($event)"
    ></app-tabbar>
    
    <app-users-gmap [city]="activeCountry?.label"></app-users-gmap>
    
    <pre>{{activeCountry?.desc}}</pre>
  `,
})
export class HomeComponent {
  countries = [
    { id: 1001, label: 'italy', desc: 'bla bla bla 1'},
    { id: 1002, label: 'germany', desc: 'bla bla bla 2'},
    { id: 1003, label: 'egypt', desc: 'bla bla bla 3'}
  ];
  activeCountry = this.countries[0];

  setActiveCountry(country): void {
    this.activeCountry = country;
  }
}
