export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F' | null;
  city?: string;
  birthday: number;
  bitcoins: number;
}
